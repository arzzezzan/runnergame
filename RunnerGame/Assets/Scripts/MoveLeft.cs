using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    private PlayerController controller;
    public float speed = 15f;
    private float leftBound = -10f;
    
    void Start()
    {
        controller = GameObject.Find("Player").GetComponent<PlayerController>();
        
    }

    void Update()
    {
        if (controller.isGameOver == false)
        {
            if (controller.doubleSpeed)
            {
                transform.Translate(Vector3.left * Time.deltaTime * (speed * 2));
            }
            else
            {
                transform.Translate(Vector3.left * Time.deltaTime * speed);
            }
        }
        if(transform.position.x < leftBound && gameObject.CompareTag("Obstacle"))
            Destroy(gameObject);
    }
}
