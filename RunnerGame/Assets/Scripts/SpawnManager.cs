using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] obstacles;
    private int randomObs;

    public Vector3 spawnPosition;

    private float firstSpawnTime = 1f;
    private float repeatSpawnTime = 1.5f;

    private PlayerController playerController;
    void Start()
    {
        
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();  
        InvokeRepeating("SpawnObstacle", firstSpawnTime, repeatSpawnTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void SpawnObstacle()
    {
        spawnPosition = new Vector3(Random.Range(20f, 28f), 0f, 0f);
        if (playerController.isGameOver == false)
        {
            randomObs = Random.Range(0, obstacles.Length);
            Instantiate(obstacles[randomObs], spawnPosition, obstacles[randomObs].transform.rotation);
        }
    }
}
